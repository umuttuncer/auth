var apm = require('elastic-apm-node').start({

  // Override the service name from package.json
  // Allowed characters: a-z, A-Z, 0-9, -, _, and space
  serviceName: 'auth',
  
  // Use if APM Server requires a secret token
  secretToken: 'zYIXjUUIhspPP8Gmw7',
  
  // Set the custom APM Server URL (default: http://localhost:8200)
  serverUrl: 'https://774f4bfc1dbb4d72bb6fb0dacce9959f.apm.europe-west3.gcp.cloud.es.io:443',
  
  // Set the service environment
  environment: 'development'
  })
  
  

import { NestFactory } from '@nestjs/core';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.createMicroservice<MicroserviceOptions>(
    AppModule,
    {
      transport:Transport.TCP,
      options:{
        port:3004
      }
    }
  );
  app.listen()
}
bootstrap();
